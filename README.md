Presentation for this task:
=============================

###Web application on Flask + Redis that prints the origin public IP of any request it receives in reverse



      https://optimovetask.top/

![](images/site_examle.png)

------------
###Build docker images (FlaskApp and Redis in different images) 



![](images/docker_images.png)

------------
###Created helm files
Also could check in GitLab


      https://gitlab.com/gamefeget/optimove-helm-testing/-/tree/master/optimove-helm

![](images/helm_charts.png)


------------
###CI/CD Pipeline in GitLab 
* Described in .gitlab-ci.yml file

      https://gitlab.com/gamefeget/optimove-helm-testing/-/blob/master/.gitlab-ci.yml

* Autodeploy both applications via pipeline to GKE using helm
* Pipeline steps:
1) Build docker and push to gitlab registry
![](images/registry.png)
2) Install secret tls from ssl certificate
3) Install Metrics Adapter for custom GKE metric
4) Deploy applications using helm to GKE

------------
###Autoscaling
* Configuration using external metric 

![](images/autoscale.png)

* Example of scaling after send loop requests to application

![](images/requests.png)

* Example of scaling after send loop requests to application

![](images/scale4.png)

* Events of autoscale by metric

![](images/scale3.png)

* Downscale because no requests

![](images/downscale.png)

------------
###Configured Let’s Encrypt ssl certificate for domain optimovetask.top

![](images/ssl.png)

------------
###Application works only by https
Ingress annotation 

      kubernetes.io/ingress.allow-http: "false"


#############################################################################
Created by Vladyslav Fedun

#############################################################################
Feel free to write me

      gamefeget@gmail.com


#############################################################################

